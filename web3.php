<!DOCTYPE HTML>  
<html>
<head>
<style>
.error {color: #FF0000;}
</style>
</head>
<body>  

<?php
$ForenameErr = $SurnameErr = $UsernameErr = $PasswordErr = $YearofbirthErr = $EmailErr = "";
$Forename = $Surname = $Username = $Password = $Yearofbirth = $Email = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["Forename"])) {
    $ForenameErr = "Required";
  } else {
    $Forename = test_input($_POST["Forename"]);
    if (strlen($Forename)<3) {
      $ForenameErr = "Forename must have at least 3 alphabet characters"; 
    }
  }
} 
if ($_SERVER["REQUEST_METHOD"] == "POST") 
  {
    if (empty($_POST["Surname"])) {
      $SurnameErr = "Required";
    } else {
      $Surname = test_input($_POST["Surname"]);
      if (strlen($Surname)<3) {
        $SurnameErr = "Surname must have at least 3 alphabet characters"; 
      }
    }
  }
if ($_SERVER["REQUEST_METHOD"] == "POST") 
  {
    if (empty($_POST["Username"])) {
      
    } else {
      $Username = test_input($_POST["Username"]);
      if (strlen($Username)<5) {
        $UsernameErr = "At least 5 characters"; 
      }
    }
  }
  if ($_SERVER["REQUEST_METHOD"] == "POST") 
  {
    if (empty($_POST["Password"])) {
     
    } else {
      $Password = test_input($_POST["Password"]);
      if (strlen($Password)<8) {
        $PasswordErr = "At least 8 characters"; 
      }
    }
  }
  if ($_SERVER["REQUEST_METHOD"] == "POST") 
  {
    if (empty($_POST["Year of birth"])) {
      
    } else {
      $Yearofbirth = test_input($_POST["Year of birth"]);
      if( strtotime($Yearofbirth) < (time() - (18 * 60 * 60 * 24 * 365))) {
        $YearofbirthErr="Allowed";
      } else {
        $YearofbirthErr="Not Allowed";
      }
    
    }
  }
  if ($_SERVER["REQUEST_METHOD"] == "POST") 
  {
    if (empty($_POST["Email"])) {
      
    } else {
      $Email = test_input($_POST["Email"]);
      if (!preg_match('/^[@]+$/i',$Email)) {
        $EmailErr = "Invalid email"; 
      }  
    }
  }
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<h1>PHP Form Validation!</h1>
<p><span class="error">* required field</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Forename: <input type="text" name="Forename" value="<?php echo $Forename;?>">
  <span class="error">* <?php echo $ForenameErr;?></span>
  <br><br>
  Surname: <input type="text" name="Surname" value="<?php echo $Surname;?>">
  <span class="error">* <?php echo $SurnameErr;?></span>
  <br><br>
  Username: <input type="text" name="Username" value="<?php echo $Username;?>">
  <span class="error"> <?php echo $UsernameErr;?></span>
  <br><br>
  Password: <input type="text" name="Password" value="<?php echo $Password;?>">
  <span class="error"> <?php echo $PasswordErr;?></span>
  <br><br>
  Yearofbirth: <input type="text" name="Yearofbirth" value="<?php echo $Yearofbirth;?>">
  <span class="error">* <?php echo $YearofbirthErr;?></span>
  <br><br>
  Email: <input type="text" name="Email" value="<?php echo $Email;?>">
  <span class="error">* <?php echo $EmailErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</form>

<?php
echo "<h2>Your Input:</h2>";
echo $Forename;
echo "<br>";
echo $Surname;
echo "<br>";
echo $Username;
echo "<br>";
echo $Email;
echo "<br>"


?>
 
 
</body>
</html>